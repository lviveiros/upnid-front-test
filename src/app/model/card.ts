export interface ICard {
  cardid: string,
  cardnumber: string,
  cvv: string,
  company: string,
  expiration_date: string
}

export interface ICardCreate {
  cardnumber: string,
  cvv: string,
  company: string,
  expiration_date: string
}

export interface ICardUpdate {
  cardid: string,
  cardnumber: string,
  cvv: string,
  company: string,
  expiration_date: string
}