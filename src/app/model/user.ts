export interface IUser{
    userid: string,
    name: string,
    surname: string,
    email: string,
    role: string
}

export interface IUserCreate{
    name: string,
    surname: string,
    email: string,
    password: string
}

export interface IUserUpdate{
    userid: string,
    name: string,
    surname: string,
    email: string,
}