import { RegisterPage } from './page/register/register.page';
import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { MainComponent } from 'app/main.component'

import { PermissionGuard, IPermissionGuardModel } from 'angular2-permission'

import * as Guard from 'app/guard'
import * as Page from 'app/page'

const appRoutes: Routes = [
  {
    path: '', canActivate: [Guard.AppGuard], children: [
      {
        path: 'auth', children: [
          { path: '', component: Page.AuthPage },
          { path: 'register', component: Page.RegisterPage },
          { path: '**', redirectTo: '', pathMatch: 'full' }
        ],
        canActivate: [PermissionGuard], data: {
          Permission: {
            Except: ['admin', 'member'],
            RedirectTo: '/main'
          } as IPermissionGuardModel
        }
      },
      {
        path: 'main', component: MainComponent, children: [
          { path: 'cards', component: Page.CardsPage },
          { path: 'my-account', component: Page.MyAccountPage },
          { path: '', redirectTo: 'cards', pathMatch: 'full' },
          { path: '**', redirectTo: 'cards', pathMatch: 'full' }
        ],
        canActivate: [PermissionGuard], data: {
          Permission: {
            Only: ['admin', 'member'],
            RedirectTo: '/auth'
          } as IPermissionGuardModel
        }
      },
      { path: '', redirectTo: 'auth', pathMatch: 'full' },
      { path: '**', redirectTo: 'auth', pathMatch: 'full' }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { useHash: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }