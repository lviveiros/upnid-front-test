import { NgModule } from '@angular/core'

import { AppComponent } from 'app/app.component'
import { MainComponent } from 'app/main.component'

import { AppRoutingModule } from 'app/app.routing.module'
import { CoreModule } from 'app/core/core.module'
import { ServiceModule } from 'app/service/service.module'
import { PageModule } from 'app/page/page.module'
import { ComponentModule } from 'app/component/component.module'
import { ModalModule } from 'app/modal/modal.module'

import * as Guard from 'app/guard'

@NgModule({
  declarations: [
    AppComponent,
    MainComponent
  ],
  imports: [
    ServiceModule.forRoot(),
    CoreModule,
    AppRoutingModule,
    PageModule,
    ComponentModule,
    ModalModule
  ],
  providers: [
    Guard.AppGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
