import { CardViewEditPopupPage, CardViewCreatePopupPage } from 'app/modal'
import { Component, OnInit } from '@angular/core'

import * as format from 'date-fns/format'

import { CardService, ModalsService, AlertMessage } from 'app/service'

@Component({
  selector: 'app-cards-page',
  templateUrl: './cards.page.html',
  styleUrls: [
      './cards.page.scss'
  ]
})
export class CardsPage implements OnInit {

  public cards: any = []
  public loading = false

  constructor(private cardService: CardService, 
            private modalsService: ModalsService,
            private alertMessage: AlertMessage) {
  }

  ngOnInit(){
   this.getCards();
  }


private getCards(){
this.loading = true;
    this.cardService.getCards().subscribe(data =>{
      if(data && data.data){
        this.cards = data.data
      }
      this.loading = false;
    }, err =>{
      this.loading = false;
    });
}
  public onDelete = (card) => {
    this.delete(card)
  }

  public onEdit = (card) => {
    this.edit(card)
  }

  public newCard = () => {
    this.create()
  }
  

  delete(card) {
    const resolve = {
      title: 'Card delete',
      message: 'Are you sure?',
      submit: (result) => {
        if (result) {
          this.cardService.deleteCard(card.cardid).subscribe(
            data => {
              this.cards = this.cards.filter(u => u !== card)
                this.alertMessage.addAlert({ type: 'success', message:'Card deleted successfuly' })
            },
            error => {
            this.alertMessage.addAlert({ type: 'danger', message:'Failed to delete card successfuly' })
            }
          )
        }
      }
    }
    this.modalsService.openMessage(resolve)
  }

edit(card){
  this.modalsService.openForm(
      CardViewEditPopupPage,
      {
        cardid: card['cardid'],
        cardnumber: card['cardnumber'],
        cvv: card['cvv'],
        company: card['company'],
        expiration_date: format(card['expiration_date'], 'YYYY-MM-DD')
      },
      (result) => {
        this.getCards();
      }
    )
}

create(){
  this.modalsService.openForm(
      CardViewCreatePopupPage,
      {
        cardnumber: '',
        cvv: '',
        company: '',
        expiration_date: ''
      },  
      (result) => {
        console.log('result: ', result);
        this.getCards();
      }
    )
}

}