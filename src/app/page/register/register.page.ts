import { IUserCreate } from './../../model/user';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'app/service'

@Component({
  selector: 'app-register-page',
  templateUrl: './register.page.html',
  styleUrls: [
    './register.page.scss'
  ]
})
export class RegisterPage {

user:IUserCreate = <IUserCreate>{};

  constructor(private userService: UserService, 
              private router: Router) {
  }


  register() {
    this.userService.newUser(this.user).
    subscribe(data => {
      this.router.navigate(['/auth'])
    })
  }

}