import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SessionService } from 'app/service'

@Component({
  selector: 'app-auth-page',
  templateUrl: './auth.page.html',
  styleUrls: [
    './auth.page.scss'
  ]
})
export class AuthPage {


  public EmailFocus: boolean
  public PasswordFocus: boolean
  public Email: string
  public Password: string

  constructor(private sessionService: SessionService, private router: Router) {

  }

  setEmailFocus(focus) {
    this.EmailFocus = focus
  }

  setPasswordFocus(focus) {
    this.PasswordFocus = focus
  }

  login() {
    this.sessionService.login({ email: this.Email, password: this.Password }).then(data => {
      this.router.navigate(['/main'])
    })
  }

}