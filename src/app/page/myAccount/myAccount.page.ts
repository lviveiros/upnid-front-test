import { UserProfilePopupPage } from 'app/modal';
import { Component, OnInit } from '@angular/core'
import { SessionService, ModalsService } from 'app/service'


@Component({
  selector: 'app-my-account-page',
  templateUrl: './myAccount.page.html',
  styleUrls: [
      './myAccount.page.scss'
  ]
})
export class MyAccountPage implements OnInit{

  constructor(public sessionService: SessionService,
              private modalsService: ModalsService) {
  }

  ngOnInit(){

  }


edit(){
  this.modalsService.openForm(
      UserProfilePopupPage,
      {
        userid: this.sessionService.me['userid'],
        name: this.sessionService.me['name'],
        surname: this.sessionService.me['surname'],
        email: this.sessionService.me['email']
      },
      (result) => {
        if (result.token) {
          this.sessionService.auth.setToken(result.token)
        }
        this.sessionService.start()
      }
    )
}

}