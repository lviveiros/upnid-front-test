import { MyAccountPage } from './myAccount/myAccount.page';
import { CardsPage } from './cards/cards.page';
import { ComponentModule } from 'app/component/component.module';
import { NgModule } from '@angular/core'
import { CoreModule } from 'app/core/core.module'
import { ModalModule } from 'app/modal/modal.module'

import * as Page from 'app/page'

@NgModule({
  imports: [
    CoreModule,
    ComponentModule,
    ModalModule
  ],
  declarations: [
    Page.AuthPage,
    Page.CardsPage,
    Page.MyAccountPage,
    Page.RegisterPage,
  ],
  exports: [
    Page.AuthPage,
    Page.CardsPage,
    Page.MyAccountPage,
    Page.RegisterPage,
  ]
})
export class PageModule { }