import { Injectable } from '@angular/core'
import { Observable } from 'rxjs/Rx'
import { HttpClient } from '@angular/common/http'


import { environment } from 'environments/environment'

@Injectable()
export class CardService {
        
  constructor(private http: HttpClient) {

  }

  getCards(): Observable<any> {
    return this.http.get(environment.ApiURL + '/cards')
  }

 deleteCard(cardId): Observable<any> {
    return this.http.delete(environment.ApiURL + '/card/'+cardId)
  }

  editCard(cardId, cardInput): Observable<any> {
    return this.http.put(environment.ApiURL + '/card/' + cardId, cardInput)
  }

  newCard(cardInput): Observable<any> {
    return this.http.post(environment.ApiURL + '/card', cardInput)
  }
}