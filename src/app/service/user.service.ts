import { Injectable } from '@angular/core'
import { Observable } from 'rxjs/Rx'
import { HttpClient } from '@angular/common/http'


import { environment } from 'environments/environment'

@Injectable()
export class UserService {
        
  constructor(private http: HttpClient) {

  }

  editUser(userId, userInput): Observable<any> {
    return this.http.put(environment.ApiURL + '/user/' + userId, userInput)
  }

 newUser(userInput): Observable<any> {
    return this.http.post(environment.ApiURL + '/user', userInput)
  }

 deleteUser(userId): Observable<any> {
    return this.http.delete(environment.ApiURL + '/user/' + userId)
  }

}