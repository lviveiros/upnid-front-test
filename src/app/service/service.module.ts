import {NgModule, ModuleWithProviders} from '@angular/core'
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http'

import * as Services from 'app/service'
import * as Interceptor from 'app/service/interceptor'
import { CoreModule } from 'app/core/core.module'

@NgModule({
  imports: [
    HttpClientModule,
    CoreModule
  ]
})
export class ServiceModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ServiceModule,
      providers: [
        {provide: HTTP_INTERCEPTORS, useClass: Interceptor.AuthInterceptor, multi: true},
        Services.AlertMessage,
        Services.AuthService,
        Services.SessionService,
        Services.CardService,
        Services.ModalsService,
        Services.UserService
      ]
    }
  }
}
