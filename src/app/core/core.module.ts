import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'

import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { CustomConfig, Ng2UiAuthModule } from 'ng2-ui-auth'
import { Ng2Permission } from 'angular2-permission'
import { GravatarModule } from 'ng2-gravatar-directive'

import { environment } from 'environments/environment'

export class AuthConfig extends CustomConfig {
  defaultHeaders = {'Content-Type': 'application/json'}
  tokenName = 'accessToken'
  tokenPrefix = ''
  baseUrl = environment.ApiURL
}

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    RouterModule,
    NgbModule.forRoot(),
    Ng2UiAuthModule.forRoot(AuthConfig),
    Ng2Permission,
    GravatarModule
  ],
  exports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    RouterModule,
    NgbModule,    
    Ng2UiAuthModule,
    Ng2Permission,
    GravatarModule
  ]
})
export class CoreModule {}