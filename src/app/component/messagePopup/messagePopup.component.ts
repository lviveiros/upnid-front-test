import { Component, Input } from '@angular/core'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-messagePopup-component',
  templateUrl: './messagePopup.component.html',
  styleUrls: [
      './messagePopup.component.scss'
  ]
})
export class MessagePopupComponent {

  @Input() title;
  @Input() message;

  constructor(private activeModal: NgbActiveModal) {

  }

  submit() {
    this.activeModal.close(true)
  }

  cancel() {
    this.activeModal.dismiss()
  }

}
