import { NgModule } from '@angular/core'

import { CoreModule } from 'app/core/core.module'

import * as Component from 'app/component'

@NgModule({
  imports: [
    CoreModule
  ],
  declarations: [
    Component.CardComponent,
    Component.CardViewComponent,
    Component.HeaderComponent,
    Component.MessagePopupComponent,
    Component.AlertMessageComponent
  ],
  entryComponents: [
    Component.MessagePopupComponent
  ],
  exports: [
    Component.CardComponent,
    Component.CardViewComponent,
    Component.HeaderComponent,
    Component.MessagePopupComponent,
    Component.AlertMessageComponent
  ]
})
export class ComponentModule {}