import { ICard } from 'app/model';
import { Component, Input } from '@angular/core'

@Component({
  selector: 'app-card-component',
  templateUrl: './card.component.html',
  styleUrls: [
      './card.component.scss'
  ]
})
export class CardComponent {
  title = 'CardComponent';

  @Input() cards: Array<ICard>
  @Input() loading: any

  @Input() onDelete: any
  @Input() onEdit: any
  @Input() newCard: any
 
  constructor() {

  }

  public delete(card) {
    this.onDelete(card)
  }

  public edit(card) {
    this.onEdit(card)
  }
 
  public create() {
    this.newCard()
  }

}
