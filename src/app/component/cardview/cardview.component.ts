import { Component, Input } from '@angular/core'

@Component({
  selector: 'app-cardview-component',
  templateUrl: './cardview.component.html',
  styleUrls: [
      './cardview.component.scss'
  ]
})
export class CardViewComponent {
  title = 'CardViewComponent';

  @Input() cards: any
  @Input() loading: any

  @Input() onDelete: any
 
  constructor() {

  }

  public delete(card) {
    this.onDelete(card)
  }

}
