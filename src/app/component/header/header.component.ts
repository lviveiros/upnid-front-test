import { SessionService } from 'app/service';
import { Component } from '@angular/core'
import { Router } from '@angular/router'

@Component({
  selector: 'app-header-component',
  templateUrl: './header.component.html',
  styleUrls: [
      './header.component.scss'
  ]
})
export class HeaderComponent {

  constructor(public sessionService:SessionService, public router:Router) {
  }



  logout() {
    this.sessionService.logout();
  }

}
