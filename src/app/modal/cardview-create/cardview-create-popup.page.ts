import { ICardCreate } from 'app/model/card';
import { Component, Input } from '@angular/core'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { CardService, AlertMessage } from 'app/service'

@Component({
  selector: 'app-cardview-create-popup-page',
  templateUrl: './cardview-create-popup.page.html'
})
export class CardViewCreatePopupPage {

cvvValidate :RegExp 

public card:ICardCreate = <ICardCreate>{}
  @Input()
  set data(data){
    this.card = data
  }

  constructor(private activeModal: NgbActiveModal, 
            private cardService: CardService,
            private alertMessage: AlertMessage) {

    this.cvvValidate=new RegExp('^[0-9]{3}$')
  }

  submit = () => {
    this.cardService.newCard(this.card).subscribe(
      result => {
      this.alertMessage.addAlert({ type: 'success', message: 'Card Created'})
        this.activeModal.close(result)
      }
    )
  }

  cancel = () => {
    this.activeModal.dismiss()
  }
}
