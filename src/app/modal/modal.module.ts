import { NgModule } from '@angular/core'

import { CoreModule } from 'app/core/core.module'

import * as Modal from 'app/modal'

@NgModule({
  imports: [
    CoreModule,
  ],
  declarations: [
    Modal.UserProfilePopupPage,
    Modal.CardViewEditPopupPage,
    Modal.CardViewCreatePopupPage,
  ],
  entryComponents: [
    Modal.UserProfilePopupPage,
    Modal.CardViewEditPopupPage,
    Modal.CardViewCreatePopupPage,
  ],
  exports: [
    Modal.UserProfilePopupPage,
    Modal.CardViewEditPopupPage,
    Modal.CardViewCreatePopupPage,
  ]
})
export class ModalModule { }
