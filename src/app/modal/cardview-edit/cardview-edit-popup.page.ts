import { ICardUpdate } from 'app/model/card';
import { Component, Input } from '@angular/core'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { CardService, AlertMessage } from 'app/service'


@Component({
  selector: 'app-cardview-edit-popup-page',
  templateUrl: './cardview-edit-popup.page.html'
})
export class CardViewEditPopupPage {


public card:ICardUpdate = <ICardUpdate>{}
  @Input()
  set data(data){
    this.card = data
  }

  constructor(private activeModal: NgbActiveModal, 
             private cardService: CardService,
             private alertMessage: AlertMessage) {
  }

  submit = () => {
    this.cardService.editCard(this.card['cardid'], this.card).subscribe(
      result => {
        this.alertMessage.addAlert({ type: 'success', message: 'Card Updated'})
        this.activeModal.close(result)
      }
    )
  }

  cancel = () => {
    this.activeModal.dismiss()
  }
}
