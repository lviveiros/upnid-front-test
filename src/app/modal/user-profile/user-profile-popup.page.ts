import { IUser } from 'app/model';
import { Component, Input } from '@angular/core'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { UserService, AlertMessage } from 'app/service'

@Component({
  selector: 'app-user-profile-popup-page',
  templateUrl: './user-profile-popup.page.html'
})
export class UserProfilePopupPage {

  public passwordA: string
  public user: IUser = <IUser>{}
  public passwordB: string
  
  @Input()
  set data(data){
    this.user = {
      'userid': data['userid'],
      'name': data['name'],
      'surname': data['surname'],
      'email': data['email'],
      'role': data['role'] 
    }
  }


  constructor(private activeModal: NgbActiveModal, 
              private alertMessage: AlertMessage,
              private userService: UserService) {
    
  }

  submit = () => {
    this.userService.editUser(this.user['userid'], this.user).subscribe(
      result => {
         this.alertMessage.addAlert({ type: 'success', message: 'User Updated'})
        this.activeModal.close(this.user)
      }
    )
  }

  cancel = () => {
    this.activeModal.dismiss()
  }
}
