# Upnid Frontend Test - Leonardo Viveiros

Angular 5 app built with Bootstrap 4  

- This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.0.
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

## Up and Running
The Angular App can be found [Here](https://upnid-front-test.herokuapp.com/)


### Prerequisites

- Software you need installed prior to execute the project

```
node
npm
```

### Installing

- A step by step series of examples that tell you have to get a development env running

#### Clonning repository

```
git clone 
```

#### Downloading Dependencies

```
npm install
```

#### Start the app in Development mode

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build --aot -prod` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Built With

* [Gravatar](https://www.npmjs.com/package/ng2-gravatar-directive) - Gravatar library
* [NgBootstrap](https://ng-bootstrap.github.io) - Ng Bootstrap component
* [Date-fns](https://github.com/date-fns/date-fns) - Date Time conversion library
* [Bootstrapious Boilerplate Template](https://bootstrapious.com/) - Bootstrap template


## Author

* **Leonardo Viveiros** 
